﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class OnPayroll : Developer
    {
        string Dept;
        string Manager;
        double NetSalary;
        byte Exp;
        double TotalSalary;

        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter Department:");
            Dept = Console.ReadLine();
            Console.WriteLine("Enter Manager Name");
            Manager = Console.ReadLine();
            Console.WriteLine("Enter Salary");
            NetSalary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter experience");
            Exp = byte.Parse(Console.ReadLine());
            Calculatepayment(NetSalary, Exp);
            DisplayDetails();
        }
        public void Calculatepayment(double netSalary, byte exp)
        {
            if (exp > 10)
            {
                TotalSalary = (((10 / 100) * NetSalary) + ((8.5 / 100) * NetSalary) - 6500 + NetSalary);
            }
           
            else
            {
                TotalSalary = (((1.9 / 100) * NetSalary) + ((2.0 / 100) * NetSalary) - 1200 + NetSalary);
            }

        }
        public void DisplayDetails()
        {
            Console.WriteLine("Basic Salary {0}", NetSalary);
            Console.WriteLine("Total salary{0}", TotalSalary);
        }
    }
}
